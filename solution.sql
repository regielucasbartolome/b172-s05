-- 1. Return the customerName of the customers who are from the Philippines.
SELECT customerName from customers WHERE country = "Philippines";

-- 2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts".
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

-- 3. Return the product name and MSRP of the product named "The Titanic".
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

-- 4. Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com".
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

-- 5. Return the names of customers who have no registered state.
SELECT customerName FROM customers WHERE state IS NULL;

-- 6. Return the first name, last name, email of the employees whose last name is Patterson and first name is Steve.
SELECT firstName, lastName, email FROM employees where lastName = "Patterson" AND firstName = "Steve";
-- 7. Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000.
SELECT customerName, country, creditLimit FROM customers where country != "USA" AND creditLimit > 3000;

-- 8. Return the customer names of customers whose customer names don't have 'a' in them.
SELECT customerName from customers WHERE customerName LIKE "%a%";

-- 9. Return customer numbers of orders whose comments contain the string 'DHL'.
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

-- 10. Return the product lines whose text description mentions the phrase 'state of the art'.
SELECT * FROM productlines WHERE textDescription LIKE "%state of the art%";

-- 11. Return the countries of customers without duplication.
SELECT DISTINCT country FROM customers;

-- 12. Return the statuses of orders without duplication.
SELECT DISTINCT status FROM orders;

-- 13. Return the customer names and countries of customers whose country is USA, France, or Canada.
SELECT customerName, country FROM customers WHERE country = "USA" OR country = "France" OR country = "Canada";

-- 14. Return the first name, last name, and office's city of employees whose offices are in Tokyo.
SELECT employees.firstName, employees.lastName, offices.city FROM employees
	JOIN offices ON employees.officeCode = offices.officeCode
    WHERE offices.officeCode = 5;

-- 15. Return the customer names of customers who were served by the employee named "Leslie Thompson".
SELECT customers.customerName FROM customers
	JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
    WHERE employees.employeeNumber = 1166;

-- 16. Return the product names and customer name of products ordered by "Baanne Mini Imports".
SELECT products.productName, customers.customerName FROM customers
	JOIN orders ON customers.customerNumber = orders.customerNumber
    JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
    JOIN products ON orderdetails.productCode = products.productCode
    WHERE customers.customerName = "Baane Mini Imports";

-- 17. Return the employees' first names, employees' last names, customers' names, and offices' countries of transaction whose customers and offices are in the same country.
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM employees, customers, offices
WHERE (customers.country = offices.country OR employees.employeeNumber = customers.salesRepEmployeeNumber)

-- 18. Return last names and first names of employees being supervised by "Anthony Bow".
SELECT lastName, firstName FROM employees 
WHERE reportsTo = (SELECT employeeNumber FROM employees WHERE firstName = "Anthony" AND lastName = "Bow");

-- 19. Return the product name and MSRP of the product with the highest MSRP
SELECT productName, MSRP FROM products ORDER BY MSRP DESC LIMIT 1;

-- 20. Return the number of customers in the UK.
SELECT COUNT(*) FROM customers WHERE country = "UK";

-- 21. Return the number of products per product line.
SELECT productLine, COUNT(*) FROM products GROUP BY productLine;

-- 22. Return the number of customers served by every employee.
SELECT COUNT(*), employees.lastName, employees.firstName FROM customers 
JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber 
GROUP BY customers.salesRepEmployeeNumber;

-- 23. Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000.
SELECT productName, quantityInStock from products WHERE productLine = "planes" AND quantityInStock < 1000;